new Vue({
  el: '#exercise',
  data: {
    value: ''
  },
  methods: {
    updateValue: function ({target}) {
      this.value = target.value;
    },
  },
});
