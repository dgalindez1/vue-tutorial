new Vue({
  el: '#app',
  data: {
    name: 'Diego',
    counter: 0,
    result: '',
    link: 'http://google.com',
  },
  computed: {
    output: function () {
      return this.counter > 5 ? 'Greater than 5' : 'Smaller than 5';
    },
  },
  watch: {
    // Can be used for async stuffies
    counter: function(value) {
      setTimeout(() => this.counter = 0, 2000);
    },
  },
  methods: {
    increase: function () {
      this.counter++;
      this.result = this.counter > 5 ? 'Greater than 5' : 'Smaller than 5';
    },
    decrease: function () {
      this.counter--;
      this.result = this.counter > 5 ? 'Greater than 5' : 'Smaller than 5';
    },
    changeLink: function () {
      this.link = 'http://youtube.com';
    },
  }
});
