new Vue({
  el: '#app',
  data: {
    attachRed: false,
    color: 'green',
    width: 100,
  },
  computed: {
    demoClasses: function () {
      return {
        red: this.attachRed,
        blue: !this.attachRed,
      }
    },
    aStyle: function () {
      return {
        backgroundColor: this.color,
        width: `${this.width}px`,
      };
    }
  },
  watch: {
  },
  methods: {
  }
});
