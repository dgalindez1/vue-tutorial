new Vue({
  el: '#app',
  data: {
    title: 'Hello World!',
    link: 'http://google.com',
    finishedLink: '<a href="http://google.com">Raw HTML Output</a>',
  },
  methods: {
    changeTitle: function ({target}) { this.title = target.value; } ,
    sayHello: function () {
      return this.title;
    },
  }
});
