new Vue({
  el: '#exercise',
  data: {
    name: 'Diego',
    age: 25,
    imageUrl: 'https://images.dog.ceo/breeds/schipperke/n02104365_5970.jpg',
  },
  methods: {
    random: () => Math.random(),
  }
});
